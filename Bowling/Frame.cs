﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling
{
    using NUnit.Framework.Constraints;

    public class Frame
    {
        private readonly bool _isLastFrame;
        public int ThrowsCount { get; set; }

        public bool IsStrike { get; set; }

        public uint Score { get; set; }

        public bool IsSpare { get; set; }

        public Frame(bool isLastFrame)
        {
            _isLastFrame = isLastFrame;
        }

        public void Knocks(uint pinsCount)
        {
            if (pinsCount + Score > 10 && !_isLastFrame )
            {
                throw new ArgumentException("Can't knock down that many pins");    
            }

            if (ThrowsCount == 2 && !IsSpare && !IsStrike)
            {
                throw new InvalidOperationException("can't throw more than 2 times if not the last frame");
            }

            ThrowsCount++;
            
            Score += pinsCount;
            if (Score == 10)
            {
                if (ThrowsCount == 1) IsStrike = true;
                if (ThrowsCount == 2) IsSpare = true;
            }
        }

        public uint GetScore()
        {
            return Score;
        }

        public void AddBonus(uint bonus)
        {
            if (!IsSpare && !IsStrike)
            {
                throw new InvalidOperationException("cannot add bonus if not spare or strike");
            }
            Score += bonus;
        }

        public bool ExceptMoreThrows()
        {
            throw new NotImplementedException();
            //   return ThrowsCount == 1 || (ThrowsCount == 2 && _isLastFrame && (IsSpare || IsStrike));
        }
    }
}
