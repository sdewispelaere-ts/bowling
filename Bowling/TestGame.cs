﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling
{
    using NUnit.Framework;

    [TestFixture]
    public class TestGame
    {
        private Game _game;

        [SetUp]
        public void Setup()
        {
            _game = new Game();
        }

        [Test]
        public void Game_WithOneThrow_KnockingDownZeroPins_ScoreIsZero()
        {
            _game.AddThrow(0);

            Assert.That(_game.Score, Is.EqualTo(0));
        }

        [Test]
        public void Game_GetInitialFrameScore_IsZero()
        {
            for(int i = 0; i < 10; i++)
                Assert.That(_game.GetFrameScore(i), Is.Null);
        }

        [Test]
        public void TwentyThrowsThatKnockOnePin_ScoreOfEachFrameIs()
        {
            for (int i = 0; i < 20; i++)
            {
                _game.AddThrow(1);
            }

            for (int i = 0; i < 10; i++)
            {
                Assert.AreEqual((i + 1) * 2, _game.GetFrameScore(i));
            }
        }
    }
}
