﻿namespace Bowling
{
    using NUnit.Framework.Constraints;

    public class Game
    {
        public uint Score { get; set; }

        private Frame[] Frames = new Frame[10];

        private int currentFrameIndex;

        public Game()
        {
            for (int i = 0; i < Frames.Length; i++)
            {
                Frames[i] = new Frame(i == Frames.Length - 1);
            }
        }

        public void AddThrow(uint pinsDown)
        {

            Frames[currentFrameIndex].Knocks(pinsDown);

        }

        public int? GetFrameScore(int i)
        {
            return null;
        }
    }
}