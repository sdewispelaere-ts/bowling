﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling
{
    using NUnit.Framework;

    [TestFixture]
    public class TestFrame
    {
        private Frame _frame;

        [SetUp]
        public void Setup()
        {
            _frame = new Frame(false);
        }

        [Test]
        public void KnockMoreThan10PinsIsAnError()
        {
            Assert.Throws<ArgumentException>(() => _frame.Knocks(11));
        }

        [TestCase((uint)1)]
        public void ScoreIsTheNumberOfPinsKnockedDown(uint pins)
        {
            _frame.Knocks(pins);
            Assert.AreEqual(pins, _frame.GetScore());
        }

        [Test]
        public void FrameWithMoreThan10PinsKnockedIsAnError()
        {
            _frame.Knocks(9);
            Assert.Throws<ArgumentException>(() => _frame.Knocks(3));
        }

        [TestCase((uint)1, (uint)2)]
        [TestCase((uint)0, (uint)2)]
        [TestCase((uint)1, (uint)8)]
        public void TwoTriesWithLessThan10PinsKnocked_ReturnSumOfPins(uint try1, uint try2)
        {
            _frame.Knocks(try1);
            _frame.Knocks(try2);

            Assert.AreEqual(try1+try2, _frame.GetScore());
        }

        [TestCase((uint)1, (uint)9)]
        [TestCase((uint)0, (uint)10)]
        [TestCase((uint)2, (uint)8)]
        public void TwoTries10PinsIsSpare(uint try1, uint try2)
        {
            _frame.Knocks(try1);
            _frame.Knocks(try2);

            Assert.IsTrue(_frame.IsSpare);
        }

        [Test]
        public void OneThrowKnockingDown10PinsIsAStrike()
        {
            _frame.Knocks(10);

            Assert.That(_frame.IsSpare, Is.False);
            Assert.That(_frame.IsStrike, Is.True);
        }

        [Test]
        public void TriesToThrowThreeTimesAndNotLastFrame_ThrowsException()
        {
            _frame.Knocks(1);
            _frame.Knocks(1);
            Assert.Throws<InvalidOperationException>(() => _frame.Knocks(1));
        }

        [Test]
        public void ThrowsThreeTimeInLastFrame_WithoutSpare_ThrowsInvalidOperationException()
        {
            _frame = new Frame(true);

            _frame.Knocks(1);
            _frame.Knocks(1);
            Assert.Throws<InvalidOperationException>(() => _frame.Knocks(1));
        }

        [Test]
        public void ThrowsThreeTimeInLastFrame_WithSpare_Succeed()
        {
            _frame = new Frame(true);

            _frame.Knocks(1);
            _frame.Knocks(9);
            _frame.Knocks(1);

            Assert.Pass();
        }

        [TestCase((uint)1, (uint)9, (uint)3)]
        [TestCase((uint)0, (uint)10, (uint)3)]
        [TestCase((uint)2, (uint)8, (uint)3)]
        public void AddBonusScoreToFrameAfterSpare_ReturnsScoreOfFrameWithBonus(uint try1, uint try2, uint bonus)
        {
            _frame.Knocks(try1);
            _frame.Knocks(try2);
            _frame.AddBonus(bonus);

            Assert.AreEqual(try1+try2+bonus, _frame.GetScore());
        }

        [TestCase((uint)1, (uint)8, (uint)3)]
        [TestCase((uint)9, (uint)0, (uint)3)]
        public void AddBonusIfNotSpareOrStrikeIsAnError(uint try1, uint try2, uint bonus)
        {
            _frame.Knocks(try1);
            _frame.Knocks(try2);
            Assert.Throws<InvalidOperationException>(() => _frame.AddBonus(bonus));
        }
    }
}
